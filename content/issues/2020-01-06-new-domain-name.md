---
section: issue
title: New Domain Name
date: 2020-01-06T19:12:27.570Z
resolved: true
resolvedWhen: 2020-01-06T19:12:27.630Z
affected:
  - Home
  - Rss-Bridge
  - AllTube
  - Keeweb
  - TT-Rss
  - IPMagnet
  - MyTap3
  - EchoIP
severity: disrupted
---
# New Domain Name 

### tap3.eu



Thank's to netlib.re <3
