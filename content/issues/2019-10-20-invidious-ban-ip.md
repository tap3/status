---
section: issue
title: invidious BAN IP
date: 2019-10-20T17:49:27.865Z
resolved: true
resolvedWhen: 2019-10-21T01:14:28.007Z
affected:
  - Invidious
severity: disrupted
---
Update: le forçage de l'IPV6 semble avoir résolu le problème, a voir combien de temps ça va tenir.  

Malheuresement google a banni "beaucoup" d'IP dont celle la, le service reste accessible met aucune videos ne peut etre lu. 

 _https://github.com/omarroth/invidious/issues/811_

_https://github.com/ytdl-org/youtube-dl/issues/21729_
