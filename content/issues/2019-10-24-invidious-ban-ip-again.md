---
section: issue
title: Invidious Ban Ip (again)
date: 2019-10-24T10:23:27.026Z
resolved: true
resolvedWhen: 2020-01-06T14:20:27.130Z
affected:
  - Invidious
severity: down
---
Youtube (Google) a de [nouveau banni mon ip](https://status.valhalla.netlib.re/issues/2019-10-20-invidious-ban-ip/) même en forçant l'IPV6 ça ne passe plus_, je vais stopper le service en attendant une solution._


_https://github.com/omarroth/invidious/issues/811_

_https://github.com/ytdl-org/youtube-dl/issues/21729_

# Arret Du Service
