---
section: issue
title: php update
date: 2019-12-03T12:47:37.309Z
resolved: true
resolvedWhen: 2019-12-03T12:47:37.449Z
affected:
  - alltube
  - rss-bridge
severity: notice
---
_Alltube now requires at least PHP 7.1_

_Rss-Bridge now requires at least PHP 7.3_
