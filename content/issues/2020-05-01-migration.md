---
section: issue
title: Migration
date: 2020-05-01T07:49:49.238Z
resolvedWhen: 2020-05-02T07:49:49.378Z
affected:
  - Home
  - AllTube
  - Rss-Bridge
  - TT-Rss
  - Keeweb
  - EchoIP
  - IPMagnet
severity: down
---
Les services vont être migrés sur un autre serveur ainsi que passés sous docker dans les jours qui viennent.

---
The services will be migrated to another server as well as moved under docker in the coming days.
